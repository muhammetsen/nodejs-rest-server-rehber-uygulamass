
phoneValidation = (req,res,next) => {
    const { phones } = req.body;

    if (!(phones)) {
        res.status(400).send({
            slug: "all_input_required",
            message: "All input is required"
        });
    }

    var phoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    phones.forEach(function(phone){
        if(!phone.match(phoneNo)) {
            res.status(400).send({
                slug: "phone_invalid",
                message: "Phone number invalid! The number format should be like this XXX-XXX-XXXX or XXX.XXX.XXXX or XXX XXX XXXX"
            });
            return;
        }
    });
    next();
}

const validation = {
    "phoneValidation": phoneValidation
}

module.exports = validation;