const jwt = require('jsonwebtoken')
const db = require("../models");
const User = db.User;
verifyToken = (req, res, next) => {

    let token = req.headers["x-access-token"];
    if (!token) {
        return res.status(403).send({
            slug: "no_token",
            message: "No token provided!"
        });
    }

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                slug: "unauthorized",
                message: "Unauthorized!"
            });
        }
        User.findOne({
            where: {
                id: decoded.user_id
            }
        }).then(user => {
            if(user){
                req.userId = decoded.user_id;
                next();
            }else{
                return res.status(404).send({
                    slug: "not_found",
                    message: "User Not Found!"
                });
            }

        }).catch(error => {
            return res.status(500).send({
                slug: "error",
                message: error
            });
        })
    });
};

const authJwt = {
    verifyToken: verifyToken,
};
module.exports = authJwt;