const db = require('../models')
const User = db.User;

controlDuplicateEmail = (req,res,next) => {
    const { first_name, last_name, email, password } = req.body;
    if (!(email && password && first_name && last_name)) {
        res.status(400).send({
            slug: "all_input_required",
            message: "All input is required"
        });
    }

    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (user) {
            res.status(400).send({
                slug: "email_already_use",
                message: "Failed! Email is already in use!"
            });
            return;
        }
        next();
    });
};

const verifySignUp = {
    "controlEmailDuplicate": controlDuplicateEmail
}

module.exports = verifySignUp;