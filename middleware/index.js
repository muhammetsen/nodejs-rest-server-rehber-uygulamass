const verifySignUp = require("./SingUp");
const authJwt = require("./AuthJwt");
const contactValid = require('./Contact')
module.exports = {
    verifySignUp,
    authJwt,
    contactValid,
};