const express = require('express')
const bodyParser = require('body-parser')
const contactRoute = require('./routes/contact')
const userRoute = require('./routes/user')

require("dotenv").config();

const app = express();
app.use(bodyParser.json())

app.use('/User',userRoute)
app.use('/Contact',contactRoute)

module.exports = app;

