const express = require('express')
const { authJwt, contactValid } = require("../middleware");
const contactController = require('../controllers/ContactController')
const router = express.Router();

router.post(
    '/CreateContact',
    [
        authJwt.verifyToken,
        contactValid.phoneValidation
    ],
    contactController.store
)

router.post(
    '/UpdateContact',
    [
        authJwt.verifyToken,
        contactValid.phoneValidation
    ],
    contactController.store
)

router.post(
    '/DeleteContact',
    [
        authJwt.verifyToken,
    ],
    contactController.deleteContact
)

router.post(
    '/ListContacts',
    [
        authJwt.verifyToken,
    ],
    contactController.listContacts
)

router.post(
    '/GetContactPhones',
    [
        authJwt.verifyToken,
    ],
    contactController.getContactPhones
)

router.post(
    '/SearchContacts',
    [
        authJwt.verifyToken,
    ],
    contactController.searchContacts
)

module.exports = router;