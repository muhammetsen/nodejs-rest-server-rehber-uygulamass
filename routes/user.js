const express = require('express')
const { verifySignUp } = require("../middleware");
const userController = require('../controllers/UserController')
const router = express.Router();

router.post(
    '/Register',
    [
        verifySignUp.controlEmailDuplicate
    ],
    userController.register
)
router.post('/Login',userController.login)

module.exports = router;