'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Users', [{
      first_name: 'John',
      last_name: 'Doe',
      email: 'admin@example.com',
      password: '$2a$10$Yg0h1BSfnzq//H/7d5dFveBs2WJCh5Er25ioL4Oc2rs8.grl2uh5e',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
