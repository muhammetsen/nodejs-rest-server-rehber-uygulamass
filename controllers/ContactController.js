const db = require('../models')
const sequelize = require('sequelize')
const Contact = db.Contact;
const Phone = db.Phone;

function store(req,res){

    const { name, surname, company, phones } = req.body;

    if (!(name && surname && company && phones)) {
        res.status(400).send("All input is required");
    }

    let phoneData = [];

    if('contactId' in req.body){ // UPDATE
        phones.forEach(function(phone){
            phoneData.push({
                "contact_id" : req.body.contactId,
                "phone" : phone,
            })
        });

        contactData = {
            "name": name,
            "surname": surname,
            "company": company,
        }
        Contact.update(
            contactData,
            {where: {id : req.body.contactId}}
            ).then(contact => {
            Phone.destroy({
                where: {
                    'contact_id': req.body.contactId
                }
            }).then( _=>{
                Phone.bulkCreate(phoneData).then(_=> {
                    res.status(201).json({
                        "status" : 1,
                        "slug" : "success",
                        "message" : "Successfully",
                        "data" : contact,
                    })
                }).catch(error => {
                    res.status(500).json({
                        "status" : 0,
                        "slug": "error",
                        "message": "error",
                        "data" : error
                    });
                });
            }).catch(error => {
                res.status(500).json({
                    "status" : 0,
                    "slug": "error",
                    "message": "error",
                    "data" : error
                });
            });
        }).catch(error => {
            res.status(500).json({
                "status" : 0,
                "slug": "error",
                "message": "error",
                "data" : error
            });
        });

    }else{ // CREATE
        Contact.create(contactData).then(contact => {

            phones.forEach(function(phone){
                phoneData.push({
                    "contact_id" : contact.id,
                    "phone" : phone,
                })
            });

            Phone.bulkCreate(phoneData).then(_=> {
                res.status(201).json({
                    "status" : 1,
                    "slug" : "success",
                    "message" : "Successfully",
                    "data" : contact,
                })
            }).catch(error => {
                res.status(500).json({
                    "status" : 0,
                    "slug": "error",
                    "message": "error",
                    "data" : error
                });
            });

        }).catch(error => {
            res.status(500).json({
                "status" : 0,
                "slug": "error",
                "message": "error",
                "data" : error
            })
        })
    }

}

function deleteContact(req,res){
    const { contactId } = req.body;
    if (!(contactId)) {
        res.status(400).send("All input is required");
    }
    Phone.destroy({
        where: {
            'contact_id': contactId
        }
    }).then( _=>{
        Contact.destroy({
            where: {
                'id': contactId
            }
        }).then(_ => {
            res.status(201).json({
                "status" : 1,
                "slug" : "success",
                "message" : "Successfully",
            })
        }).catch(error => {
            res.status(500).json({
                "status" : 0,
                "slug": "error",
                "message": "error",
                "data" : error
            });
        });
    }).catch(error => {
        res.status(500).json({
            "status" : 0,
            "slug": "error",
            "message": "error",
            "data" : error
        });
    });

}

async function listContacts(req,res){
    const { contactId } = req.body;
    if (!(contactId)) {
        res.status(400).send("All input is required");
    }

    let contacts = await Contact.findAll({
        where: {
            'user_id': req.userId
        }
    });

    let retData = [];

    contacts.forEach(function(contact){
        retData.push({
            "id": contact.id,
            "name": contact.name,
            "surname": contact.surname,
            "company": contact.company,
        })
    });

    res.status(201).json({
        "status" : 1,
        "slug" : "success",
        "message" : "Successfully",
        "data" : retData,
    });

}

async function getContactPhones(req,res){
    const { contactId } = req.body;
    if (!(contactId)) {
        res.status(400).send("All input is required");
    }

    let phones = await Phone.findAll({
        where: {
            'contact_id': contactId
        }
    });

    let retData = [];
    phones.forEach(function(phone){
        retData.push({
            "contact_id": phone.contact_id,
            "phone": phone.phone,
        })
    });

    res.status(201).json({
        "status" : 1,
        "slug" : "success",
        "message" : "Successfully",
        "data" : retData,
    });

}

async function searchContacts(req,res){
    const { keyword } = req.body;
    if (!(keyword)) {
        res.status(400).send("All input is required");
    }

    let contacts = await Contact.findAll({
        where: {
            'user_id': req.userId,
            [sequelize.Op.or] : [
                {name: { [sequelize.Op.like]: '%' + keyword + '%' }},
                {surname:  { [sequelize.Op.like]: '%' + keyword + '%' }},
                {company:  { [sequelize.Op.like]: '%' + keyword + '%' }},
                { namesQuery: sequelize.where(
                        sequelize.fn(
                            "concat",
                            sequelize.col("name"),
                            " ",
                            sequelize.col("surname")
                        ),
                        {
                            [sequelize.Op.like]: `%${keyword}%`,
                        }
                    ),}
            ],
        }
    });

    let retData = [];

    contacts.forEach(function(contact){
        retData.push({
            "id": contact.id,
            "name": contact.name,
            "surname": contact.surname,
            "company": contact.company,
        })
    });

    res.status(201).json({
        "status" : 1,
        "slug" : "success",
        "message" : "Successfully",
        "data" : retData,
    });

}


module.exports = {
    store:store,
    deleteContact:deleteContact,
    listContacts:listContacts,
    getContactPhones:getContactPhones,
    searchContacts:searchContacts,
}