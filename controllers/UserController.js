const db = require('../models')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = db.User;

async function register(req,res){
    try {

        const { first_name, last_name, email, password } = req.body;

        encryptedPassword = bcrypt.hashSync(password, 10);

        const user = await User.create({
            first_name,
            last_name,
            email: email.toLowerCase(), // sanitize: convert email to lowercase
            password: encryptedPassword,
        });

        // Create token
        const token = jwt.sign(
            { user_id: user.id, email },
            process.env.JWT_SECRET,
            {
                expiresIn: process.env.JWT_EXPIRATION_TIME+"h",
            }
        );
        user.token = token;

        res.status(201).json(user);

    }catch (err){
        console.log(err);
    }
}

async function login(req,res){
    try {
        const { email, password } = req.body;

        if (!(email && password)) {
            res.status(400).send("All input is required");
        }
        const user = await User.findOne({
            where:{
                'email':email
            }
        });

        if (user && (await bcrypt.compare(password, user.password))) {
            const token = jwt.sign(
                { user_id: user.id, email },
                process.env.JWT_SECRET,
                {
                    expiresIn: process.env.JWT_EXPIRATION_TIME+"h",
                }
            );

            user.token = token;
            res.status(200).json(user);
        }
        res.status(400).send("Invalid Credentials");
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    register:register,
    login:login
}